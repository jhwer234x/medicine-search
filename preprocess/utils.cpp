//
//  utils.cpp
//  main
//
//  Created by DAEJIN HYEON on 11/15/14.
//  This module is for extracting and make contour image.
//
//

#include "utils.h"
using namespace cv;
using namespace std;
//using namespace std;
const int RESOLUTION = 200;

IplImage* wimg;// = cvCloneImage( wimg0 );
IplImage* wimg2;// = cvCloneImage(wimg0);
IplImage* wimg_gray;// = cvCloneImage( wimg0 );
IplImage* wshed;// = cvCloneImage( wimg0 );
IplImage* marker_mask;// = cvCreateImage( cvGetSize(wimg), 8, 1 );
IplImage* markers;// = cvCreateImage( cvGetSize(wimg), IPL_DEPTH_32S, 1 );
CvPoint prev_pt;
void on_mouse( int event, int x, int y, int flags, void* param )
{
    if( !wimg )
        return;
    
    if( event == CV_EVENT_LBUTTONUP || !(flags & CV_EVENT_FLAG_LBUTTON) )
        prev_pt = cvPoint(-1,-1);
    else if( event == CV_EVENT_LBUTTONDOWN )
        prev_pt = cvPoint(x,y);
    else if( event == CV_EVENT_MOUSEMOVE && (flags & CV_EVENT_FLAG_LBUTTON) )
    {
        CvPoint pt = cvPoint(x,y);
        if( prev_pt.x < 0 )
            prev_pt = pt;
        cvLine( marker_mask, prev_pt, pt, cvScalarAll(255), 5, 8, 0 );
        cvLine( wimg, prev_pt, pt, cvScalarAll(255), 5, 8, 0 );
        prev_pt = pt;
        cvShowImage( "image", wimg );
    }
}

CvSeq* extractContour(IplImage* raw, CvMemStorage* storage, int type = CV_CHAIN_APPROX_SIMPLE)
{
    //CvMemStorage* storage1 = cvCreateMemStorage(0);
	CvSeq* contour = 0;
    IplImage* gray = cvCreateImage(cvGetSize(raw), 8, 1);
    
	cvCvtColor(raw, gray, CV_RGB2GRAY);
    cvFindContours(gray, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, type);
    
	cvSetZero(gray);
	cvDrawContours(gray, contour, CV_RGB(255,255,255), CV_RGB(255,255,255), 0, 2, 8, cvPoint(0, 0));
    cvReleaseImage(&gray);
    return contour;
}

void getRectVerticesFromContour(CvSeq* contour, CvPoint* vertices)
{
    CvBox2D box = cvMinAreaRect2(contour);
	
	double angle = box.angle*CV_PI/180.;
	float c = (float)cos(angle)*0.5f;
	float s = (float)sin(angle)*0.5f;
	//CvPoint2D32f pt[4] = {0};
	vertices[0].x = box.center.x + s*box.size.height + c*box.size.width;//-origin_width*0.45;
	vertices[0].y = box.center.y + c*box.size.height + s*box.size.width - box.size.height*2*c;
	vertices[1].x = box.center.x - s*box.size.height - c*box.size.width +2*box.size.height*s;// - origin_width*0.45;
	vertices[1].y = box.center.y - c*box.size.height - s*box.size.width;
	vertices[2].x =  vertices[1].x - box.size.height*s*2;
	vertices[2].y = vertices[1].y + 2*c*box.size.height;
	vertices[3].x = vertices[0].x - 2*box.size.height*s;
	vertices[3].y =  vertices[0].y + box.size.height*2*c;
}
float caculatePointLineDistance(CvPoint line1, CvPoint line2, CvPoint point)
{
    CvPoint vector1 = cvPoint(line1.x - line2.x, line1.y - line2.y);
    CvPoint vector2 = cvPoint(point.x - line2.x, point.y - line2.y);
    double size1 = sqrt(pow(vector1.x,2.0)+pow(vector1.y,2.0));
    double size2 = sqrt(pow(vector2.x,2.0)+pow(vector2.y,2.0));
    double costheta = acos((vector1.x*vector2.x + vector1.y*vector2.y)/(size1*size2));
    if(abs(size2*sin(costheta))>33)
        printf("\nsize1:%f, size2:%f, costheta:%f,", size1, size2, costheta*180/CV_PI);
    return abs(size2*sin(costheta));
}
float getEculidianDistance(CvPoint point1, CvPoint point2)
{
    return sqrt(pow(point1.x-point2.x,2.0)+pow(point1.y-point2.y,2.0));
}

float calculateErrorWithRect(CvPoint* vertices, CvPoint point)
{
    CvPoint vertexL1, vertexL2;
    CvPoint vertexR1, vertexR2;
    if(getEculidianDistance(vertices[0], vertices[1]) > getEculidianDistance(vertices[1], vertices[2]))
    {
        vertexL1 = vertices[0];
        vertexL2 = vertices[1];
        vertexR1 = vertices[2];
        vertexR2 = vertices[3];
    }
    else{
        vertexL1 = vertices[1];
        vertexL2 = vertices[2];
        vertexR1 = vertices[3];
        vertexR2 = vertices[0];
    }
    
    float short_distance = getEculidianDistance(vertexL2, vertexR1);
    
    float candidate1 = caculatePointLineDistance(vertexL1,vertexL2,point);
    float candidate2 = caculatePointLineDistance(vertexR1,vertexR2,point);

    
    return min(candidate1, candidate2);///short_distance;
}
float penaltyFunction(float value, float arg0)
{
    if(abs(value)>arg0)
    {
        return abs(value-arg0)*100;
    }
    else return 0;
}
IplImage* getResizedImage(IplImage* src, float ratio)
{
    IplImage* wimg0 = cvCreateImage(cvSize(src->width*ratio, src->height*ratio), src->depth, src->nChannels);
    cvResize(src, wimg0);
	cvReleaseImage(&src);
    return wimg0;
}

IplImage* featureWatershed(IplImage* itempmg, float ratio)
{
    cvNamedWindow( "image", 1 );
    cvNamedWindow( "watershed transform", 1 );
    CvRNG rng = cvRNG(-1);
	IplImage* wimg0 = cvCreateImage(cvSize(itempmg->width*ratio, itempmg->height*ratio), itempmg->depth, itempmg->nChannels);
    cvResize(itempmg, wimg0);
	cvReleaseImage(&itempmg);
    
    
    wimg = cvCloneImage( wimg0 );
	wimg2 = cvCloneImage(wimg0);
    wimg_gray = cvCloneImage( wimg0 );
    wshed = cvCloneImage( wimg0 );
    marker_mask = cvCreateImage( cvGetSize(wimg), 8, 1 );
    markers = cvCreateImage( cvGetSize(wimg), IPL_DEPTH_32S, 1 );
    cvCvtColor( wimg, marker_mask, CV_BGR2GRAY );
    cvCvtColor( marker_mask, wimg_gray, CV_GRAY2BGR );
    
    cvZero( marker_mask );
    cvZero( wshed );
    cvShowImage( "image", wimg );
    //cvShowImage( "watershed transform", wshed );
    cvSetMouseCallback( "image", on_mouse, 0 );
    
    // for(;;)
    // {
    int c = cvWaitKey(0);
    
    //if( (char)c == 27 )
    //     break;
    
    if( (char)c == 'r' )
    {
        cvZero( marker_mask );
        cvCopy( wimg0, wimg );
        cvShowImage( "image", wimg );
    }
    
    if( (char)c == 'w' || (char)c == '\n' )
    {
        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* contours = 0;
        CvMat* color_tab;
        int i, j, comp_count = 0;
        
        cvFindContours( marker_mask, storage, &contours, sizeof(CvContour),
                       CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
        cvZero( markers );
        for( ; contours != 0; contours = contours->h_next, comp_count++ )
        {
            cvDrawContours( markers, contours, cvScalarAll(comp_count+1),
                           cvScalarAll(comp_count+1), -1, -1, 8, cvPoint(0,0) );
            
        }
        
        color_tab = cvCreateMat( 1, comp_count, CV_8UC3 );
        
        
        //	int comp_count =2;
        for(int i = 0; i < comp_count; i++ )
        {
            uchar* ptr = color_tab->data.ptr + i*3;
            ptr[0] = (uchar)(245 - 245*i);
            ptr[1] = (uchar)(245 - 245*i);
            ptr[2] = (uchar)(245 - 245*i);
        }
        
        //cvShowImage("c88", markers);
        //cvWaitKey(0);
        //wshed = cvCloneImage( cropped );
        cvWatershed( wimg0, markers );
        
        
        // paint the watershed image
        for(int i = 0; i < markers->height; i++ )
        {
            for(int j = 0; j < markers->width; j++ )
            {
                int idx = CV_IMAGE_ELEM( markers, int, i, j );
                uchar* dst = &CV_IMAGE_ELEM( wshed, uchar, i, j*3 );
                if( idx == -1 )
                    dst[0] = dst[1] = dst[2] = (uchar)255;
                else if( idx <= 0 || idx > comp_count )
                    dst[0] = dst[1] = dst[2] = (uchar)0; // should not get here
                else
                {
                    uchar* ptr = color_tab->data.ptr + (idx-1)*3;
                    dst[0] = ptr[0]; dst[1] = ptr[1]; dst[2] = ptr[2];
                }
            }
        }
    }
    
    return wshed;
}
IplImage* featureWatershed(std::string filename, float ratio)
{
	//char* filename =  (char*)"E:/pill/img/Medicine/6.jpg";
    // cvNamedWindow( "image", 1 );
    // cvNamedWindow( "watershed transform", 1 );
    CvRNG rng = cvRNG(-1);
	//cout << "check3" << endl;
	IplImage* itempmg = cvLoadImage(filename.c_str());
    return featureWatershed(itempmg, ratio);
}
IplImage* featureWatershed(char* filename, float ratio)
{
	//char* filename =  (char*)"E:/pill/img/Medicine/6.jpg";
   // cvNamedWindow( "image", 1 );
   // cvNamedWindow( "watershed transform", 1 );
    CvRNG rng = cvRNG(-1);
	//cout << "check3" << endl;
	IplImage* itempmg = cvLoadImage(filename);
    return featureWatershed(itempmg, ratio);
}

void differenceImage(IplImage* src,IplImage* dst, IplImage* result)
{
	//printf("\n width: %d, widthstep: %d", dst->width, src->widthStep);
	for(int y= 0; y<src->height;y++)
	{
		uchar* ptr = (uchar*) (src->imageData + y*src->widthStep);
		uchar* dptr = (uchar*) (dst->imageData + y*dst->widthStep);
		uchar* rptr = (uchar*) (result->imageData + y*result->widthStep);
		for(int x =0; x<dst->widthStep; x++)
		{
			rptr[x] = abs(ptr[x]-dptr[x]);
		}
	}
}

IplImage* featureStep(std::string image_path, CvSeq* contour)
{
    
	char buf[256];
    //std::string image_path = ROOT + "/Medicine/%s.jpg";
	//sprintf(buf, image_path.c_str());
    IplImage* img2 = cvLoadImage("background.jpg");
	IplImage* img = cvLoadImage( image_path.c_str());
    IplImage* wshed;
	if(img == NULL)
		return false;
	int origin_width = img->width;
	int origin_height = img->height;
	CvScalar val = cvGet2D(img, img->height/2,img->width/2);
	double* rgb = val.val;
	if((int)rgb[0] != 225 || (int)rgb[1]!=206 || (int)rgb[2]!=191)
	{
		//free(rgb);
		cvReleaseImage(&img);
		return false;
	}
	IplImage* cropped = cvCreateImage( cvSize((int)(origin_width*0.9), (int)(origin_height*0.7)),  img->depth, img->nChannels );
	IplImage* color_cropped = cvCreateImage( cvSize((int)(origin_width*0.9), (int)(origin_height*0.7)),  img->depth, img->nChannels );
	IplImage* gray = cvCreateImage(cvSize((int)(origin_width*0.9), (int)(origin_height*0.7)),IPL_DEPTH_8U,1);
	IplImage* gback = cvCreateImage(cvSize((int)(origin_width*0.5), (int)(origin_height*0.7)),IPL_DEPTH_8U,1);
	
	cvSetImageROI( img,  cvRect( origin_width*0.03,origin_height*0.15, (int)(origin_width*0.9),(int)(origin_height*0.7)));
	cvCopy( img, color_cropped );
	cvResetImageROI( img );
    
	cvCvtColor(cropped, gray, CV_RGB2GRAY);
	differenceImage(img,img2,img);;//cvSub(img, img2, img2);
	cvSetImageROI( img,  cvRect( origin_width*0.03,origin_height*0.15, (int)(origin_width*0.9),(int)(origin_height*0.7)));
	cvCopy( img, cropped );
	cvResetImageROI( img );
	cvCvtColor(cropped, gray, CV_RGB2GRAY);
	cvThreshold( gray, gray, 15.0, 255.0, CV_THRESH_BINARY);
	IplConvKernel *element;
	element = cvCreateStructuringElementEx (23, 23, 11, 11, CV_SHAPE_RECT, NULL);
	cvMorphologyEx(gray, gray, NULL, element, CV_MOP_CLOSE, 1);
    
	CvPoint2D32f tcenter= cvPoint2D32f(0,0);
	float tradius=0;
    
	cvReleaseImage(&color_cropped);
	IplImage* markers = cvCreateImage( cvGetSize(gray), IPL_DEPTH_32S, 1 );//added
	CvMemStorage* storage = cvCreateMemStorage(0);
//	CvSeq* contour = 0;
	cvFindContours(gray, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	CvScalar color = CV_RGB(255, 255, 255);
	cvSetZero(gray);
	cvSetZero(gback);
	cvZero( markers );
	cvDrawContours( markers, contour, cvScalarAll(100),
                   cvScalarAll(100), -1, -1, 8, cvPoint(0,0) );
	cvCircle(gray, cvPoint(0,0), 50, color, 2);
	cvFindContours(gray, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	cvSetZero(gray);
	cvDrawContours( markers, contour, cvScalarAll(200),
                   cvScalarAll(200), -1, -1, 8, cvPoint(0,0) );
	CvMat* color_tab;
	color_tab = cvCreateMat( 1, 2, CV_8UC3 );
	int comp_count =2;
    for(int i = 0; i < comp_count; i++ )
    {
        uchar* ptr = color_tab->data.ptr + i*3;
        ptr[0] = (uchar)(245 - 245*i);
        ptr[1] = (uchar)(245 - 245*i);
        ptr[2] = (uchar)(245 - 245*i);
    }
    
    {
        wshed = cvCloneImage( cropped );
        cvWatershed( cropped, markers );
    }
    // paint the watershed image
    for(int i = 0; i < markers->height; i++ )
        for(int j = 0; j < markers->width; j++ )
        {
            int idx = CV_IMAGE_ELEM( markers, int, i, j );
            uchar* dst = &CV_IMAGE_ELEM( wshed, uchar, i, j*3 );
            if( idx == -1 )
                dst[0] = dst[1] = dst[2] = (uchar)255;
            else if( idx <= 0 || idx > comp_count )
                dst[0] = dst[1] = dst[2] = (uchar)0; // should not get here
            else
            {
                uchar* ptr = color_tab->data.ptr + (idx-1)*3;
                dst[0] = ptr[0]; dst[1] = ptr[1]; dst[2] = ptr[2];
            }
        }
	cvCvtColor(wshed, gray, CV_RGB2GRAY);
	cvFindContours(gray, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	cvDrawContours(gback, contour, color, color, 0, 2, 8, cvPoint(-origin_width*0.45, 0));
	//cvCvtColor(gback, gray,CV_RGB2GRAY);
    
	/////
	//
	CvBox2D box = cvMinAreaRect2(contour);
	double angle = box.angle*CV_PI/180.;
	float c = (float)cos(angle)*0.5f;
	float s = (float)sin(angle)*0.5f;
	CvPoint2D32f pt[4] = {0};
	pt[0].x = box.center.x + s*box.size.height + c*box.size.width-origin_width*0.45;
	pt[0].y = box.center.y + c*box.size.height + s*box.size.width - box.size.height*2*c;
	pt[1].x = box.center.x - s*box.size.height - c*box.size.width +2*box.size.height*s - origin_width*0.45;
	pt[1].y = box.center.y - c*box.size.height - s*box.size.width;
	pt[2].x =  pt[1].x - box.size.height*s*2;
	pt[2].y = pt[1].y + 2*c*box.size.height;
	pt[3].x = pt[0].x - 2*box.size.height*s;
	pt[3].y =  pt[0].y + box.size.height*2*c;

	float candi1 = sqrt(pow((int)pt[0].x - (int)pt[1].x,2.0)+pow((int)pt[0].y - (int)pt[1].y,2.0));
	float candi2 = sqrt(pow((int)pt[1].x - (int)pt[2].x,2.0)+pow((int)pt[1].y - (int)pt[2].y,2.0));
	CvPoint2D32f dst[3] = {0};
	CvPoint2D32f src[3] = {0};
	//printf("show");
	//cvShowImage("myfirstwindow", gback);
	IplImage* pinn;// = cvCreateImage(cvSize(100, 100),8,3);
	
    //scale image to have same height.
	if(candi1 > candi2)
	{
        pinn = cvCreateImage(cvSize(RESOLUTION+100,100+RESOLUTION*(candi2/candi1)),8,1);
        dst[0].x = 50; dst[0].y = 50;
        dst[1].x = RESOLUTION+50; dst[1].y = 50;
        dst[2].x = 50; dst[2].y = RESOLUTION*(candi2/candi1)+50;
        
        src[0].x = pt[1].x;
        src[0].y = pt[1].y;
        src[1].x = pt[0].x ;
        src[1].y = pt[0].y;
        src[2].x = pt[2].x ;
        src[2].y = pt[2].y ;
	}
	else
	{
		pinn = cvCreateImage(cvSize(RESOLUTION+100,100+RESOLUTION*(candi1/candi2)),8,1);
		dst[0].x = 50; dst[0].y = 50;
        dst[1].x = RESOLUTION+50; dst[1].y = 50;
        dst[2].x = 50; dst[2].y = 50+RESOLUTION*(candi1/candi2);
		src[0].x = pt[1].x ;
		src[0].y = pt[1].y ;
		src[1].x = pt[0].x ;
		src[1].y = pt[0].y ;
		src[2].x = pt[2].x ;
		src[2].y = pt[2].y ;
	}
	float newm[9];
	CvMat mat = cvMat( 2, 3, CV_32F, newm );
	cvGetAffineTransform(src, dst,&mat);
	///cvGetPerspectiveTransform(pt, pt,&mat);
	cvWarpAffine(gback, pinn, &mat);

	cvFindContours(pinn, storage, &contour, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	cvSetZero(pinn);
	cvDrawContours(pinn, contour, color, color, 0, 1, 8, cvPoint(0, 0));
	
	
    
	IplImage* color_output = cvCreateImage(cvGetSize(pinn), 8, 3);
	cvCvtColor(pinn, color_output, CV_GRAY2RGB);
	//printf("find poly");
	CvSeq* result = cvApproxPoly(contour, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.005, 0);
	//printf("malloc points");
	CvPoint *vt = (CvPoint *)malloc(sizeof(CvPoint)*result->total);
	//printf("\ntotal points:%d\n", result->total);
	cvSetZero(pinn);
	for(int i=0;i<result->total;i++){
        vt[i] = *(CvPoint*)cvGetSeqElem(result, i);
    }
	//printf("draw points");
	for(int i=0;i<result->total-1;i++){
        //   cvLine(color_output, vt[i], vt[i+1], cvScalar(255,255,255),4);
		cvLine(pinn, vt[i], vt[i+1], cvScalar(255,255,255),4);
    }
	//printf("draw points");
	//cvLine(color_output, vt[result->total-1], vt[0], cvScalar(255,0,0),4);
	cvLine(pinn, vt[result->total-1], vt[0], cvScalar(255,255,255),4);

	
	free(vt);
	//printf("clear points");
	
	CvSeq* lines = 0;
    
	//CV_HOUGH_PROBABILISTIC MODE
	lines = cvHoughLines2(pinn, storage, CV_HOUGH_PROBABILISTIC, 1, CV_PI/180, 50, 50, 10);
	
	for(int i=0; i<lines->total; i++)
	{
		CvPoint* line = (CvPoint*)cvGetSeqElem(lines, i);
        //	cvLine(color_output, line[0], line[1], CV_RGB(255, 0, 0), 2, 8);
	}

	cvClearSeq(result);
//	cvClearSeq(contour);
	cvClearSeq(lines);
	cvClearMemStorage(storage);
	cvReleaseMemStorage(&storage);
	cvReleaseStructuringElement(&element);
	cvReleaseMat(&color_tab);
	cvReleaseImage(&gray);
	cvReleaseImage(&img);
	cvReleaseImage(&markers);
	cvReleaseImage(&wshed);
	cvReleaseImage(&cropped);
	cvReleaseImage(&gback);
	cvReleaseImage(&color_output);
//	cvReleaseImage(&pinn);
	return pinn;
}
void convertRGBtoHSV(float colors[], float hsv[])
{
    float r = (float) colors[0];
    float g = (float) colors[1];
    float b = (float) colors[2];
    float h = 0.0, s = 0.0, v = 0.0;
    
    float _r = r/255;
    float _g = g/255;
    float _b = b/255;
    
    float cMax = std::max(_r, std::max(_g, _b));
    float cMin = std::min(_r, std::min(_g, _b));
    float diffMaxMin = cMax - cMin;
    
    //Hue Calculation
    if (cMax == _r) {
        h = 60 * ( fmodf(((_g - _b) / diffMaxMin), 6) );
    } else if (cMax == _g) {
        h = 60 * ( ((_b - _r) / diffMaxMin) + 2 );
    } else {
        h = 60 * ( ((_r - _g) / diffMaxMin) + 4 );
    }
    
    if (h < 0) {
        h = 360 + h;
    }
    
    //Saturation Calculation
    if (cMax == 0) {
        s = 0;
    } else {
        s = diffMaxMin / cMax;
    }
    
    //Value Calculation
    v = cMax;
    
    hsv[0] = h;
    hsv[1] = s;
    hsv[2] = v;
}
void getRGB(IplImage* src, float* rgb) {
    int total = 0;
    float red = 0, green = 0, blue = 0;
    
    // find an average color of the pill by adding all the pixels
    for(int y= 0; y<src->height;y++)
    {
        uchar* sptr = (uchar*) (src->imageData + y*src->widthStep);
        for(int x =0; x<src->width; x++)
        {
            if(sptr[3*x]!=0 && sptr[3*x+1]!=0 && sptr[3*x+2]!=0) {
                total++;
                blue+=sptr[3*x];
                green+=sptr[3*x+1];
                red+=sptr[3*x+2];
            }
        }
    }

    //Divide by total pixels to get the average of the pill
    rgb[2] = blue/total;
    rgb[1] = green/total;
    rgb[0] = red/total;
}
void getRGB(IplImage* src, IplImage* mask, float* rgb) {
    int total = 0;
    float red = 0, green = 0, blue = 0;
    
    // find an average color of the pill by adding all the pixels
    for(int y= 0; y<src->height;y++)
    {
        uchar* sptr = (uchar*) (src->imageData + y*src->widthStep);
        uchar* mptr = (uchar*) (mask->imageData + y*mask->widthStep);
        for(int x =0; x<mask->width; x++)
        {
            // if the pixel is not masked. get the color
            if(mptr[3*x]==255 && mptr[3*x+1]==255 && mptr[3*x+2]==255) {
                total++;
                blue+=sptr[3*x];
                green+=sptr[3*x+1];
                red+=sptr[3*x+2];
            }
        }
    }
    
    //Divide by total pixels to get the average of the pill
    rgb[2] = blue/total;
    rgb[1] = green/total;
    rgb[0] = red/total;
}
void getMedianRGB(IplImage* src, int* rgb) {
    std::vector< int > red;
    std::vector< int > blue;
    std::vector< int > green;
    
    for(int y= 0; y<src->height;y++)
    {
        uchar* sptr = (uchar*) (src->imageData + y*src->widthStep);
        for(int x =0; x<src->width; x++)
        {
            if(sptr[3*x]!=0 && sptr[3*x+1]!=0 && sptr[3*x+2]!=0) {
                blue.push_back(sptr[3*x]);
                green.push_back(sptr[3*x+1]);
                red.push_back(sptr[3*x+2]);
            }
        }
    }
    
    std::sort (red.begin(), red.end());
    std::sort (blue.begin(), blue.end());
    std::sort (green.begin(), green.end());
    
    rgb[0] = red.at(red.size()/2);
    rgb[1] = green.at(green.size()/2);
    rgb[2] = blue.at(blue.size()/2);
}
void test(void)
{
    int NTRAINING_SAMPLES = 1;
    int FRAC_LINEAR_SEP = 1;
	// Data for visual representation
    const int WIDTH = 512, HEIGHT = 512;
    Mat I = Mat::zeros(HEIGHT, WIDTH, CV_8UC3);
    
    //--------------------- 1. Set up training data randomly ---------------------------------------
    Mat trainData(2*NTRAINING_SAMPLES, 2, CV_32FC1);
    Mat labels   (2*NTRAINING_SAMPLES, 1, CV_32FC1);
    
    
    RNG rng(100); // Random value generation class
    
    // Set up the linearly separable part of the training data
    int nLinearSamples = (int) (FRAC_LINEAR_SEP * NTRAINING_SAMPLES);
    
    // Generate random points for the class 1
    Mat trainClass = trainData.rowRange(0, nLinearSamples);
    // The x coordinate of the points is in [0, 0.4)
    
    
    Mat c = trainClass.colRange(0, 1);
    rng.fill(c, RNG::UNIFORM, Scalar(1), Scalar(0.4 * WIDTH));
    // The y coordinate of the points is in [0, 1)
    c = trainClass.colRange(1,2);
    
    rng.fill(c, RNG::UNIFORM, Scalar(1), Scalar(HEIGHT));
    
    // Generate random points for the class 2
    trainClass = trainData.rowRange(2*NTRAINING_SAMPLES-nLinearSamples, 2*NTRAINING_SAMPLES);
    // The x coordinate of the points is in [0.6, 1]
    c = trainClass.colRange(0 , 1);
    rng.fill(c, RNG::UNIFORM, Scalar(0.6*WIDTH), Scalar(WIDTH));
    // The y coordinate of the points is in [0, 1)
    c = trainClass.colRange(1,2);
    rng.fill(c, RNG::UNIFORM, Scalar(1), Scalar(HEIGHT));
    
    //------------------ Set up the non-linearly separable part of the training data ---------------
    
    // Generate random points for the classes 1 and 2
    trainClass = trainData.rowRange(  nLinearSamples, 2*NTRAINING_SAMPLES-nLinearSamples);
    // The x coordinate of the points is in [0.4, 0.6)
    c = trainClass.colRange(0,1);
    rng.fill(c, RNG::UNIFORM, Scalar(0.4*WIDTH), Scalar(0.6*WIDTH));
    // The y coordinate of the points is in [0, 1)
    c = trainClass.colRange(1,2);
    rng.fill(c, RNG::UNIFORM, Scalar(1), Scalar(HEIGHT));
    
    //------------------------- Set up the labels for the classes ---------------------------------
    labels.rowRange(                0,   NTRAINING_SAMPLES).setTo(1);  // Class 1
    labels.rowRange(NTRAINING_SAMPLES, 2*NTRAINING_SAMPLES).setTo(2);  // Class 2
    
    //------------------------ 2. Set up the support vector machines parameters --------------------
    CvSVMParams params;
    params.svm_type    = SVM::C_SVC;
    
    params.C           = 0.1;
    params.kernel_type = SVM::LINEAR;
    params.term_crit   = TermCriteria(CV_TERMCRIT_ITER, (int)1e7, 1e-6);
    
    //------------------------ 3. Train the svm ----------------------------------------------------
    cout << "Starting training process" << endl;
    CvSVM svm;
    svm.train_auto(trainData, labels, Mat(), Mat(), params);
    cout << "Finished training process" << endl;
    
    //------------------------ 4. Show the decision regions ----------------------------------------
    Vec3b green(0,100,0), blue (100,0,0);
    for (int i = 0; i < I.rows; ++i)
        for (int j = 0; j < I.cols; ++j)
        {
            Mat sampleMat = (Mat_<float>(1,2) << i, j);
            float response = svm.predict(sampleMat);
            
            if      (response == 1)    I.at<Vec3b>(j, i)  = green;
            else if (response == 2)    I.at<Vec3b>(j, i)  = blue;
        }
    
    //----------------------- 5. Show the training data --------------------------------------------
    int thick = -1;
    int lineType = 8;
    float px, py;
    // Class 1
    for (int i = 0; i < NTRAINING_SAMPLES; ++i)
    {
        px = trainData.at<float>(i,0);
        py = trainData.at<float>(i,1);
        circle(I, Point( (int) px,  (int) py ), 3, Scalar(0, 255, 0), thick, lineType);
    }
    // Class 2
    for (int i = NTRAINING_SAMPLES; i <2*NTRAINING_SAMPLES; ++i)
    {
        px = trainData.at<float>(i,0);
        py = trainData.at<float>(i,1);
        circle(I, Point( (int) px, (int) py ), 3, Scalar(255, 0, 0), thick, lineType);
    }
    
    //------------------------- 6. Show support vectors --------------------------------------------
    thick = 2;
    lineType  = 8;
    int x     = svm.get_support_vector_count();
    
    for (int i = 0; i < x; ++i)
    {
        const float* v = svm.get_support_vector(i);
        circle( I,  Point( (int) v[0], (int) v[1]), 6, Scalar(128, 128, 128), thick, lineType);
    }
    
    imwrite("result.png", I);                      // save the Image
    imshow("SVM for Non-Linear Training Data", I); // show it to the user
    waitKey(0);
}

int getDistance(int* ref, int* compare){
    return abs(ref[0]-compare[0])+abs(ref[1]-compare[1])+abs(ref[2]-compare[2]);
}
void add_node(int node_idx, NODE* node_array, NODE* node_list){
    //node_list->index = 0;
    //node_idx;
    //NODE nnn = node_array[node_idx];
    //   if(node_array[node_idx].index == -1)
    //   {
    node_array[node_idx].index = node_idx;
    node_array[node_idx].next = node_list->next;
    if(node_array[node_idx].next != -1)
        node_array[node_array[node_idx].next].prev = node_idx;
    node_array[node_idx].prev = node_list->index;
    node_list->next = node_idx;
    //   }
}
void merge_node(int min_class, int max_class, NODE* node_array, NODE* node_list){
    node_array[max_class].index = -1;
    if(node_array[max_class].prev !=-1)
        node_array[node_array[max_class].prev].next = node_array[max_class].next;
    else
        node_list->next = node_array[max_class].next;
    if(node_array[max_class].next !=-1)
        node_array[node_array[max_class].next].prev = node_array[max_class].prev;
    
}
int getClass(int class_idx, int* class_bin){
    int index = class_bin[class_idx];
    if(index == class_idx)
        return index;
    else
        return getClass(index, class_bin);
    return -1;
}
void getColor(IplImage* src, int x, int y, int* dst){
    uchar* sptr = (uchar*)(src->imageData + y*src->widthStep);
    dst[0] = sptr[3*x];
    dst[1] = sptr[3*x+1];
    dst[2] = sptr[3*x+2];
}
void getColor(IplImage* src, int x, int y, int* dst, int* bins, bool plus){
    getColor(src, x, y, dst);
    if(plus)
    {
        if(bins[(int)((dst[0]+dst[1]+dst[2])/3)] < 0)
            bins[(int)((dst[0]+dst[1]+dst[2])/3)] = 1;
        else
            bins[(int)((dst[0]+dst[1]+dst[2])/3)]++;
    }
    else
    {
        if(bins[(int)((dst[0]+dst[1]+dst[2])/3)] > 0)
            bins[(int)((dst[0]+dst[1]+dst[2])/3)] = -1;
        else
            bins[(int)((dst[0]+dst[1]+dst[2])/3)]--;
    }
}

IplImage* crop( IplImage* src,  CvRect roi)
{
    
    // Must have dimensions of output image
    IplImage* cropped = cvCreateImage( cvSize(roi.width,roi.height), src->depth, src->nChannels );
    
    // Say what the source region is
    cvSetImageROI( src, roi );
    
    // Do the copy
    cvCopy( src, cropped );
    cvResetImageROI( src );
    
    return cropped;
}


void clustering(NODE* node_list, IplImage* gray_image, IplImage* original_image, int** check,int* cluster_bin, int* cluster_size, float thres, NODE* node_array, int* bins, bool plus ){
    int cluster_idx = 0;
    for(int y = 0;y<gray_image->height;y++)
    {
        for(int x = 0;x<gray_image->width;x++)
        {
            //  float ref = cvGetReal2D(gray_image,y,x);
            float left;// = cvGetReal2D(gray_image, y, x-1);
            float left_class;// = cvGetReal2D(gray_image, y, x-1);// until converge
            int leftC[3] = {0,0,0};
            int upC[3] = {0,0,0};
            int refC[3] = {0,0,0};
            getColor(original_image, x, y, refC,bins, plus);
            if(x-1<0)
            {
                left = 1000;
                leftC[0] = 1000;
                leftC[1] = 1000;
                leftC[2] = 1000;
            }
            else
            {
                left = cvGetReal2D(gray_image, y, x-1);
                getColor(original_image, x-1, y, leftC);
                left_class = getClass(check[y][x-1], cluster_bin);//cvGetReal2D(cluster, y, x-1);// until converge
            }
            
            float up;// = cvGetReal2D(gray_image, y-1, x);
            float up_class;// = cvGetReal2D(gray_image, y-1, x);
            if(y-1<0)
            {
                up = 1000;
                upC[0] = 1000;
                upC[1] = 1000;
                upC[2] = 1000;
            }
            else{
                up = cvGetReal2D(gray_image, y-1, x);
                getColor(original_image, x, y-1, upC);
                up_class = getClass(check[y-1][x], cluster_bin);//cvGetReal2D(cluster, y-1, x);
            }
            
            bool lefton = false;
            if(getDistance(refC,leftC)<=thres)
            {
                // cvSetReal2D(cluster, y, x, left_class);
                check[y][x] = left_class;
                cluster_size[(int)left_class]++;
                lefton = true;
            }
            //  else
            //      cvCircle(color_image, cvPoint(x,y), 0.5, cvScalar(255,0,0));
            bool upon = false;
            if(getDistance(refC,upC)<= thres)
            {
                upon = true;
                
                
                if(lefton){
                    int min_class = min(up_class, left_class);
                    int max_class = max(up_class, left_class);
                    if(min_class == max_class)
                    {
                        // cvSetReal2D(cluster, y, x, min_class);
                        check[y][x] = min_class;
                    }
                    else{
                        //  cvSetReal2D(cluster, y, x, min_class);
                        //  cvSetReal2D(cluster, y, x-1, min_class);
                        //  cvSetReal2D(cluster, y-1, x, min_class);
                        
                        check[y][x] = min_class;
                        check[y][x-1] = min_class;
                        check[y-1][x] = min_class;
                        cluster_bin[max_class] = min_class;
                        merge_node(min_class, max_class, node_array, (node_list));
                        cluster_size[(int)min_class] += cluster_size[(int)max_class];
                    }
                    
                }
                else{
                    check[y][x] = up_class;
                    // cvSetReal2D(cluster, y, x, up_class);
                    cluster_size[(int)up_class]++;
                }
            }
            //  else
            //      cvCircle(color_image, cvPoint(x,y), 0.5, cvScalar(255,0,0));//cvSetReal2D(color_image,y,x,0);
            if(!upon && !lefton)
            {
                //  cvSetReal2D(cluster, y, x, cluster_idx);
                check[y][x] = cluster_idx;
                cluster_bin[cluster_idx] = cluster_idx;
                add_node(cluster_idx, node_array, (node_list));
                // node_list;
                cluster_size[cluster_idx]=1;
                cluster_idx++;
            }
        }
    }
}
IplImage* getStandardImage(CvPoint* vertices, IplImage* result, int width, int height)
{
    CvPoint2D32f pt[3];
    pt[0] = cvPoint2D32f(vertices[0].x, vertices[0].y);
    pt[1] = cvPoint2D32f(vertices[1].x, vertices[1].y);
    pt[2] = cvPoint2D32f(vertices[2].x, vertices[2].y);
    float distance1 = getEculidianDistance(vertices[0], vertices[1]);
    float distance2 = getEculidianDistance(vertices[1], vertices[2]);
    CvPoint2D32f dst[3];
    CvRect roi;
    roi.x = 0;
    roi.y = 0;
    if(distance1>distance2)
    {
        dst[0] = cvPoint2D32f(0.0,0.0);
        dst[1] = cvPoint2D32f(0.0,height);
        dst[2] = cvPoint2D32f(width, height);
        roi.width = width;
        roi.height = height;
        
    }
    else
    {
        dst[0] = cvPoint2D32f(0.0,0.0);
        dst[1] = cvPoint2D32f(width,0.0);
        dst[2] = cvPoint2D32f(width, height);
        roi.width = width;
        roi.height = height;
        
    }
    CvMat* warp_mat=cvCreateMat(2, 3, CV_32FC1);
    IplImage* warped_img;
    warped_img=cvCloneImage(result);
    warped_img->origin=result->origin;
    cvZero(warped_img);
    cvGetAffineTransform(pt, dst, warp_mat);
    cvWarpAffine(result, warped_img, warp_mat);
    IplImage* cropped = crop(warped_img, roi);
    cvReleaseImage(&warped_img);
    return cropped;
}
IplImage* getStandardImage(CvPoint* vertices, IplImage* result)
{
    CvPoint2D32f pt[3];
    pt[0] = cvPoint2D32f(vertices[0].x, vertices[0].y);
    pt[1] = cvPoint2D32f(vertices[1].x, vertices[1].y);
    pt[2] = cvPoint2D32f(vertices[2].x, vertices[2].y);
    float distance1 = getEculidianDistance(vertices[0], vertices[1]);
    float distance2 = getEculidianDistance(vertices[1], vertices[2]);
    CvPoint2D32f dst[3];
    CvRect roi;
    roi.x = 0;
    roi.y = 0;
    if(distance1>distance2)
    {
        dst[0] = cvPoint2D32f(0.0,0.0);
        dst[1] = cvPoint2D32f(distance1,0.0);
        dst[2] = cvPoint2D32f(distance1, distance2);
        roi.width = distance1;
        roi.height = distance2;
        
    }
    else
    {
        dst[0] = cvPoint2D32f(0.0,0.0);
        dst[1] = cvPoint2D32f(0.0,distance1);
        dst[2] = cvPoint2D32f(distance2, distance1);
        roi.width = distance2;
        roi.height = distance1;
    }
    CvMat* warp_mat=cvCreateMat(2, 3, CV_32FC1);
    IplImage* warped_img;
    warped_img=cvCloneImage(result);
    warped_img->origin=result->origin;
    cvZero(warped_img);
    cvGetAffineTransform(pt, dst, warp_mat);
    cvWarpAffine(result, warped_img, warp_mat);
    IplImage* cropped = crop(warped_img, roi);
    cvReleaseImage(&warped_img);
    return cropped;
}
IplImage* getClusteredImage(IplImage* original, float threshold)
{
    float thres = threshold;
    bool plus = false;
    
    IplImage* gray = cvCreateImage(cvGetSize(original), 8, 1);
    IplImage* color_image = cvCreateImage(cvGetSize(original), 8, 3);
    color_image = cvCloneImage(original);
    cvCvtColor(original, gray, CV_RGB2GRAY);
    int* cluster_size = (int*)malloc(sizeof(int)*(gray->height*gray->width));
    int* cluster_bin = (int*)malloc(sizeof(int)*(gray->height*gray->width));
    
    
    int* bins = (int*)malloc(sizeof(int)*(255));
    
    NODE* node_array = (NODE*)malloc(sizeof(NODE)*(gray->height*gray->width));
    
    NODE node_list;
    node_list.index =-1;
    node_list.next = -1;
    node_list.prev = -1;
    int** check = (int**)malloc(sizeof(int*) * (gray->height));
    int cluster_idx = 0;
    for(int y = 0;y<gray->height;y++)
    {
        check[y] = (int*)malloc(sizeof(int)*gray->width);
    }
    
    int total = 0;
    clustering(&node_list, gray, original, check, cluster_bin, cluster_size, thres, node_array, bins, plus);
    
    NODE* node_list1 = &node_list;
    vector<float> a;
    
    int max=0; int max2 =0;
    int maxindex, maxindex2;
    while((*node_list1).next != -1){
        
        total++;
        (node_list1) = &node_array[(*node_list1).next];
        int class_num = getClass((*node_list1).index, cluster_bin);
        int size = cluster_size[class_num];
        if(size>max)
        {
            max2 = max;
            maxindex2 = maxindex;
            max = size;
            maxindex = class_num;
        }
        else if(size > max2)
        {
            max2 = size;
            maxindex2 = class_num;
        }
        
        //if(cluster_size[node_list.index]<5)
        //    continue;
        
    }
//    printf(" (%d, %d, %d, %d) ", maxindex, max, maxindex2, max2);
    
    IplImage* out = cvCreateImage( cvGetSize(original), IPL_DEPTH_32S, 1 );
    cvZero(out);
    NODE* node_list2 = &node_list;
    total = 0;
    for(int y = 0;y<color_image->height;y++)
    {
        for(int x = 0;x<color_image->width;x++)
        {
            int cluster=getClass(check[y][x], cluster_bin) ;
            if(cluster == maxindex)
            {
                cvSetReal2D(out, y, x, 1);
            }
            else if(cluster == maxindex2)
            {
                cvSetReal2D(out, y, x, 2);
            }
        }
    }
    cvWatershed( original, out );
    
    // paint the watershed image
    for(int i = 0; i < out->height; i++ )
    {
        for(int j = 0; j < out->width; j++ )
        {
            int idx = CV_IMAGE_ELEM( out, int, i, j );
            uchar* dst = &CV_IMAGE_ELEM( color_image, uchar, i, j*3 );
            if( idx == -1 )
                dst[0] = dst[1] = dst[2] = (uchar)0;
            else if( idx <= 0 || idx > 2 )
                dst[0] = dst[1] = dst[2] = (uchar)0; // should not get here
            else
            {
               if(idx ==1)
               {
                   dst[0] = dst[1] = dst[2] = (uchar)0;
               }
               else if(idx == 2)
               {
                   dst[1] = dst[0] = dst[2] = (uchar)255;
               }
            }
        }
    }
   
    return color_image;
}
bool getPeak(IplImage* src, int y, int x, int size, float threshold)
{
    uchar* sptr = (uchar*)(src->imageData + y*src->widthStep);
    if(x < size)
        return false;
    if(src->width<=x+size)
        return false;
    
    if(sptr[x] <= sptr[x-1] || sptr[x] <= sptr[x+1])
        return false;
    
    int ref = 0;
	int max = 0;
    ref =sptr[x];//+sptr[3*x+1]+sptr[3*x+2];
    int neighbor = 0;
    
	for(int i=x-size;i<=x;i++)
    {
		max = MAX(sptr[i], max);
        neighbor += sptr[i];//+sptr[3*i+1]+sptr[3*i+2];
    }
	float s = ref - neighbor/(float)(size+1);
	if(s < threshold)
        return false;
	if(max != ref)
		return false;
    
	neighbor = 0;
	max = 0;
	for(int i=x;i<=x+size;i++)
    {
		max = MAX(sptr[i], max);
        neighbor += sptr[i];//+sptr[3*i+1]+sptr[3*i+2];
    }
	s = ref - neighbor/(float)(size+1);
	if(s < threshold)
        return false;
	if(max != ref)
		return false;
    
	
	return true;
}
void removeLight(IplImage* src, int threshold)
{
    /*
     Input:
        src: gray image
        threshold: threshold
     */
    float mean;
    int total = 0;
    for(int i=0;i<src->width;i++)
    {
        for(int j=0;j<src->height;j++)
        {
            total += cvGetReal2D(src, j, i);
        }
    }
    mean = total/(src->width*src->height);
    for(int i=0;i<src->width;i++)
    {
        for(int j=0;j<src->height;j++)
        {
            int value = cvGetReal2D(src, j, i);
            if(value - mean > threshold)
            {
                int count = 0;
                int sum = 0;
                int temp;
                temp = cvGetReal2D(src, max(0,j-1), i);
                if(temp-mean<=threshold)
                {
                    count++;
                    sum += temp;
                }
                temp = cvGetReal2D(src, j, max(i-1,0));
                if(temp-mean<=threshold)
                {
                    count++;
                    sum += temp;
                }
                temp = cvGetReal2D(src, min(j+1, src->height-1), i);
                if(temp-mean<=threshold)
                {
                    count++;
                    sum += temp;
                }
                temp = cvGetReal2D(src, j, min(i+1, src->width-1));
                if(temp-mean<=threshold)
                {
                    count++;
                    sum += temp;
                }
                if(count!=0)
                    cvSetReal2D(src, j, i, sum/count);
            }
        }
    }
    
}
void findColorFromHSV(float* hsv) {
    if (hsv[2]*100 <= 20) {
        printf("Black");
    } else if (hsv[1]*100 <= 10) {
        printf("White");
    } else {
//        red 0
//        orange 30
//        yellow 60
//        blue 240
//        pink 325
//        green 100
//        red 360
        std::vector< int > colorChart;
        colorChart.push_back(0);
        colorChart.push_back(30);
        colorChart.push_back(60);
        colorChart.push_back(100);
        colorChart.push_back(240);
        colorChart.push_back(325);
        colorChart.push_back(360);
        
        int value = abs(hsv[0] - colorChart[0]);
        int num = 0;
        
        for(int x = 0;x < colorChart.size(); x++)
        {
            if(value > abs(hsv[0] - colorChart[x]))
            {
                value = abs(hsv[0] - colorChart[x]);
                num = colorChart[x];
            }
        }
        printf("^^^^^%d^^^^",num);
    }
}