//
//  unit_test.cpp
//  main
//
//  Created by DAEJIN HYEON on 11/15/14.
//
//

#include "unit_test.h"
#include "preprocess/utils.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
using namespace std;
using namespace cv;
const bool CONTOUR_TEST = true;
const bool DEMO = false;
const bool CLUSTER_TEST = false;
const string ROOT = "/Users/daejinhyeon/development/workspace/pill/medicine/code";
const char *homedir;


int main(){
    homedir = getenv ("HOME");
    //printf(pPath);// << endl;
    std::string image_path;
    //cout << image_path << endl;

    if(CLUSTER_TEST)
    {
        IplImage* itempmg = cvLoadImage("~/medicine-images/new/IMG_20141201_110454.jpg");
        cvShowImage("test", itempmg);
        int k = cvWaitKey(1);
        while(k==27)
        {
            k = cvWaitKey(1);
        }
        image_path = image_path + "~/medicine-images/new/IMG_20141201_110454.jpg";
        IplImage* result = featureWatershed("~/medicine-images/new/IMG_20141201_110454.jpg",1);
        
        ///Users/daejinhyeon/Google Drive/pill/images/new/IMG_20141201_110454.jpg
    }
    if(CONTOUR_TEST)
    {
        image_path = homedir;
        printf("\nstart");
        image_path = image_path + "/medicine-images/new/IMG_20141201_110454.jpg";
        cout << image_path <<endl;
        //const char* path = image_path.c_str();
        IplImage* result = featureWatershed(image_path,0.2);//"~/medicine-images/new/IMG_20141201_110454.jpg",0.2);
        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* seq = extractContour(result, storage);
        CvPoint vertices[4];
        getRectVerticesFromContour(seq, vertices);
        
        IplImage* testimage = cvCreateImage(cvGetSize(result), 8, 3);
        cvZero(testimage);
        cvLine(testimage, vertices[0], vertices[1], cvScalar(0,0,255));
        cvLine(testimage, vertices[1], vertices[2], cvScalar(0,0,255));
        cvLine(testimage, vertices[2], vertices[3], cvScalar(0,0,255));
        cvLine(testimage, vertices[3], vertices[0], cvScalar(0,0,255));
        cvShowImage("testimage", testimage);
        
        CvPoint2D32f center= cvPoint2D32f(0,0);
        float radius=0;
        cvMinEnclosingCircle(seq, &center,  &radius);
        CvPoint *vt = (CvPoint *)malloc(sizeof(CvPoint)*seq->total);

        CvPoint prev_pt = *(CvPoint*)cvGetSeqElem(seq, seq->total-1);

        CvPoint2D32f prev = *(CvPoint2D32f*)cvGetSeqElem(seq, seq->total-1);
        //cv::calc
        //vector<Mat> radiuses;
       // cvMa
        
        CvMat* radiuses =cvCreateMat(360,1, CV_32FC1);
        int row = 0;
        float test = 0;
        double mean, std, variance;
        mean = 0;
        std = 0;
        variance = 0;
        int total = 0;
        vector<double> rrr;
        printf("\n seq total:%d", seq->total);
        CvPoint2D32f temp2;
        int hist[700];
        cvCircle(result, cvPoint(center.x, center.y), 3, cvScalar(0,255,0));
        for(int i=0;i<700;i++)
            hist[i] = 0;
        
        for(int i=0;i<seq->total;i++){
            
            vt[i] = *(CvPoint*)cvGetSeqElem(seq, i);
            float dot_product = (vt[i].x - center.x)*(prev_pt.x - center.x) + (vt[i].y - center.y)*(prev_pt.y - center.y);
            float cosTheta = dot_product/(sqrt(pow(vt[i].x - center.x,2.0)+pow(vt[i].y - center.y,2.0))*sqrt(pow(prev_pt.x - center.x,2.0)+pow(prev_pt.y - center.y,2.0)));
            float theta = acos(cosTheta)*180/CV_PI;
            
            if(theta < 1)
                continue;
            test += theta;
       //     printf("\ntest:%f",test);
            float alpha = 0;
            CvPoint temp1;
            for(int k=0;k<=theta;k++)
            {
                
                total++;
                alpha = k/theta;
               // CvPoint a = cvPoint(1.1,2.2);
                CvPoint2D32f temp = cvPoint2D32f((1-alpha)*(prev_pt.x - center.x) + (alpha)*(vt[i].x - center.x), (1-alpha)*(prev_pt.y - center.y)+(alpha)*(vt[i].y - center.y));
                temp1 = cvPoint(((1-alpha)*(prev_pt.x)+(alpha)*(vt[i].x)),((1-alpha)*(prev_pt.y)+(alpha)*(vt[i].y)));
                
                temp2 = cvPoint2D32f(((1-alpha)*(prev_pt.x)+(alpha)*(vt[i].x)),((1-alpha)*(prev_pt.y)+(alpha)*(vt[i].y)));
                
                if(((int)theta) == k)
                    break;
             //   printf("\n temp.x:%f, temp.y:%f ", temp.x, temp.y);
                
                float error = calculateErrorWithRect(vertices, temp1);
                if(error>33)
                    printf("\n e   rror:%f, x:%d, y:%d ", error, temp1.x, temp1.y);
                hist[row] = error*error;
                
                double value = error*error;
                
                /*
                if(temp2.x == prev.x && temp2.y == prev.y)
                    ;
                else{
                    CvPoint2D32f temp3 = cvPoint2D32f(temp2.x - prev.x,temp2.y - prev.y);
                    double thetat = acos(temp3.x/sqrt(pow(temp3.x,2.0)+pow(temp3.y,2.0)));
                    if(thetat<0)
                    {
                        thetat += 180;//thetat;
                    }
                    thetat = 180*thetat/CV_PI; // collect histogram of theta diff.
                    hist[(int)thetat]++;
                }*/
                prev = temp2;
                
               // double value = sqrt(pow(temp.x,2.0)+pow(temp.y,2.0));
                rrr.push_back(value);
                mean += value;
                variance += value*value;
              //  cvmSet(radiuses, row, 0, value);
              //  printf("\n row:%d ", row);
                row++;
                

                cvCircle(result, cvPoint((float)((1-alpha)*((float)prev_pt.x) + (alpha)*(vt[i].x)), (1-alpha)*(prev_pt.y )+(alpha)*(vt[i].y )), 2, cvScalar(0,0,255));
            }
            printf("\nstart2: %d", i);
            
            prev_pt = temp1;//vt[i];
           
        }
        
        mean = mean/row;
        printf("\nmean:%f, variance:%f, row:%d", mean, variance,row);
        variance = variance/row;
        variance = variance - mean*mean;
        
        double variant=0;
        for(int p=0;p<rrr.size();p++)
        {
            variant += pow(rrr.at(p)-mean,2.0);
        }
        variant = variant/total;
        std = sqrt(variance);
        printf("mean:%f, std:%f", mean, std);
    //    cvZero(result);
        for(int p=0;p<seq->total-1;p++){
            //        cvLine(color_output, vt[i], vt[i+1], cvScalar(255,0,0),4);
       //     cvCircle(result, vt[i], 2, cvScalar(0,0,255));
           // cvLine(pinn, vt[i], vt[i+1], cvScalar(255,0,0),4);
        }
        
        
     //   cvCircle(result, cvPoint(center.x, center.y), radius, cvScalar(255,0,0));
        
        IplImage* histogram = cvCreateImage(cvSize(700, 400), 8, 1);
        cvZero(histogram);
        for(int p=0;p<700;p++)
        {
            cvLine(histogram, cvPoint(p,399), cvPoint(p,399-hist[p]), cvScalar(255));
        }
        cvLine(histogram, cvPoint(row, 0), cvPoint(row, histogram->height-1), cvScalar(255));
        cvShowImage("hisrogram", histogram);
        cvShowImage("water", result);
        cvClearMemStorage(storage);
        cvReleaseMemStorage(&storage);
        int k = cvWaitKey(1);
        while(k != 27)
        {
            k = cvWaitKey(1);
        }

        //CvSeq* contour = 0;
        //IplImage* img = featureStep(image_path, contour);
    }
    return 1;
}